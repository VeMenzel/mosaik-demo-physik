# Installation and startup instructions 

```
docker build -t="simulation" .   
docker run -d -p 8000:8000 simulation
```

After some time, web visualization becomes visible at localhost:8000.

Manual start: 

```
pip install -r requirements.txt
python demo.py

```` 

# Configuration Data
(Most information from https://gitlab.com/mosaik/components/energy/mosaik-pypower)
 
Configuration data for this scenario can be found in data/demo_lv_grid.json.

```  
bus: [name, type, Base voltage [kV]]
 e.g. [ ["node_b2",  "PQ", 0.23] ]
        
["<trafo_id>", "<from_bus_id>", "<to_bus_id>", <Sr_MVA>, <v1_%>, <P1_MW>, <Imax_p_A>, <Imax_s_A>]
 e.g. ["transformer", "tr_pri", "tr_sec", 0.25, 4.2, 0.00275, 6.9, 360.8]

["<branch_id>", "<from_bus_id>", "<to_bus_id>", <length_km>, <R'_ohm/km>, <X'_ohm/km>, <C'_nF/km>, <I_max_A>]
 e.g. ["branch_1",  "tr_sec",   "node_a1",  0.100, 0.2542, 0.080425, 0.0, 240.0]

```

data/profiles.data.gz contains the household load profiles <br>
pv_10kw.csv the PV panel data 


If you need any help, want an introduction to this example or simply want to talk through some ideas on how to adapt this for your use case, please do not hesitate to send me an email to v.m.menzel@utwente.nl. 
