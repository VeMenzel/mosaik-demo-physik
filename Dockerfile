FROM python:3.9-slim-bullseye

RUN apt-get update && apt-get -y install  \
    gcc \
    libhdf5-dev \
    libopenblas-dev \
    liblapack-dev

# upgrade pip
RUN /usr/local/bin/python -m pip install --upgrade pip

# install requiremets
COPY . . 
WORKDIR .
RUN pip install -r requirements.txt

# start simulation
EXPOSE 8000 
ENTRYPOINT ["python", "./demo.py"]